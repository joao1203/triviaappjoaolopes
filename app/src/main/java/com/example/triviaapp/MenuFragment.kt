package com.example.triviaapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.triviaapp.databinding.FragmentMenuBinding
import com.example.triviaapp.databinding.FragmentMenuBinding.inflate

class MenuFragment : Fragment() {

    lateinit var binding: FragmentMenuBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = inflate(inflater,container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)
        binding.playbutton.setOnClickListener {
            val action = MenuFragmentDirections.actionMenuFragment2ToGameScreenFragment2()
            findNavController().navigate(action)

        }
        binding.rankbutton.setOnClickListener {
            val action = MenuFragmentDirections.actionMenuFragment2ToRankFragment()
            findNavController().navigate(action)
        }
    }

}